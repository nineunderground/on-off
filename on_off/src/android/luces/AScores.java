/**
On-Off Android app. Basic game where you must switch off all lights
    Copyright (C) 2010  nineunderground@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package android.luces;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.luces.utils.DeviceValues;
import android.luces.utils.Navigation;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TableLayout.LayoutParams;

public class AScores extends Activity {

	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	
    	super.onCreate(savedInstanceState);
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Navigation.setFullScreenMode(this);
        
        setContentView(R.layout.puntuaciones);
        
        // Se resalta la ultima puntuacion
        Intent currentIntent	=	getIntent();
    	int	lastScore			=	currentIntent.getIntExtra("last_score", 0); // El primero de todos es el cero, y por defecto tambien
    	
    	//Log.d(this.getClass().getName(), "LA ULTIMA PUNTUACION ES LA NUMERO: " + lastScore);
    	
        aplicarEstilos();
        cargarDatosPuntuaciones(lastScore);
        cargarEventos();
        
        setContentLanguage();
    }

    /**
     * 
     */
	private void aplicarEstilos() {
		
		ScrollView	scrollPuntos	=	(ScrollView)findViewById(R.id.ScrollViewPuntuaciones);
		
		android.view.ViewGroup.LayoutParams	paramsScroll		=	scrollPuntos.getLayoutParams();
		
		paramsScroll.height = DeviceValues.heigth - 144;
		
		scrollPuntos.setLayoutParams(paramsScroll);
		
	}
	
	/**
	 * Carga los marcadores en el scroll view
	 */
	private void cargarDatosPuntuaciones(int lastPositionScore){

		/* Find Tablelayout defined in main.xml */
		TableLayout tl = (TableLayout)findViewById(R.id.TablaPuntuaciones);
	    
		TableRow tr;
		
		// 5 celdas que ocupan el 100% de ancho de la pantalla:
		
		double ancho = 0;
		ancho = DeviceValues.width * 0.1;
		
		int test	=	0;
		test = (int)ancho;
		
		Log.d(this.getClass().getName(), "PROBANDO ANCHO CALCULADO, DEBE SER " + ancho + " ?? = 20 \n");
		Log.d(this.getClass().getName(), "PROBANDO ANCHO CALCULADO, DEBE SER " + test + " ?? = 20 \n");
		
		int anchoCelda1 = (int)(DeviceValues.width * 0.1);   // 10%
		int anchoCelda2 = (int)(DeviceValues.width * 0.2);   // 20%		
		int anchoCelda3 = (int)(DeviceValues.width * 0.05);   // 5%
		int anchoCelda4 = (int)(DeviceValues.width * 0.55);   // 55%
		int anchoCelda5 = DeviceValues.width - (anchoCelda1 + anchoCelda2 + anchoCelda3 + anchoCelda4); // (resto, aprox. 10%)
		
		TextView	celda1;
		TextView	celda2;
		TextView	celda3;
		TextView	celda4;
		TextView	celda5;
		
		for(int cont=0; cont<ALuces.gameScores.getScores(); cont++){
			
		    /* Create a new row to be added. */
		    tr = new TableRow(this);
		    
		    tr.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.FILL_PARENT));
		    //tr.setBackgroundColor(R.color.electric_green);
		    
		    celda1 = new TextView(this);
		    celda2 = new TextView(this);
		    celda3 = new TextView(this);
		    celda4 = new TextView(this);
		    celda5 = new TextView(this);
		    
		    celda1.setHeight(30);
		    celda2.setHeight(30);
		    celda3.setHeight(30);
		    celda4.setHeight(30);
		    celda5.setHeight(30);
		    
		    celda1.setWidth(anchoCelda1);
		    celda1.setText( String.valueOf(cont+1).concat("º") );
		    celda1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
		    celda1.setTypeface(Typeface.DEFAULT_BOLD);
		    
		    // Nombres
		    celda2.setWidth(anchoCelda2);
		    celda2.setText(ALuces.gameScores.getName(cont));
		    celda2.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
		    celda2.setTypeface(Typeface.DEFAULT_BOLD);
		    
		    if( lastPositionScore == cont ){
		    	
		    	// La ultima puntuacion se merece estar destacada, :)
		    	//celda2.setTextColor(R.color.yellow);
		    	//celda4.setTextColor(R.color.yellow);
		    	
		    	celda1.setBackgroundResource(R.color.blue);
		    	celda2.setBackgroundResource(R.color.blue);
		    	celda3.setBackgroundResource(R.color.blue);
		    	celda4.setBackgroundResource(R.color.blue);
		    	celda5.setBackgroundResource(R.color.blue);
		    	
		    }
		    
		    celda3.setWidth(anchoCelda3);
		    celda3.setText("");
		    celda3.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
		    celda3.setTypeface(Typeface.DEFAULT_BOLD);
		    
		    // Puntuaciones
		    celda4.setWidth(anchoCelda4);//setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT));
		    celda4.setText( String.valueOf(ALuces.gameScores.getScore(cont)) );
		    celda4.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
		    celda4.setTypeface(Typeface.DEFAULT_BOLD);
		    celda4.setGravity(Gravity.RIGHT);
		    
		    celda5.setWidth(anchoCelda5);
		    celda5.setText("");
		    celda5.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
		    celda5.setTypeface(Typeface.DEFAULT_BOLD);
		    
		    /* Add cells to row. */
		    tr.addView(celda1);
		    tr.addView(celda2);
		    tr.addView(celda3);
		    tr.addView(celda4);
		    tr.addView(celda5);
		              
		    /* Add row to TableLayout. */
		    tl.addView(tr,new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.FILL_PARENT));
			
		}
		
	}
	
	/**
	 * Boton que vuelve a la pantalla principal
	 */
	private void cargarEventos(){
		
		Button btnVolver	=	(Button)findViewById(R.id.boton_volver_puntuaciones);
		
		btnVolver.setOnTouchListener( new View.OnTouchListener() {
			
			public boolean onTouch(View arg0, MotionEvent event) {
				
				if ( event.getAction() == MotionEvent.ACTION_DOWN ) {
				
					setResult(1);
					finish();
				
					return true;
				}
				
				
				return false;
			}
			
		});
		
	}
	
	//**************************************************************************************************************************************
    //
    //									T R A D U C T I O N
    //
    //**************************************************************************************************************************************

	/**
     * Sets strings according language supplied to this function
     * 
     */
    private void setContentLanguage() {
		
    	Button		botonVolver			=	(Button)findViewById(R.id.boton_volver_puntuaciones);
    	TextView	tituloPuntos	=	(TextView)findViewById(R.id.RotuloPuntuaciones);
    	
    	botonVolver.setText(Navigation.traducir(ALuces.getDiccionario(ALuces.language), "BOTON_VOLVER"));
    	tituloPuntos.setText(Navigation.traducir(ALuces.getDiccionario(ALuces.language), "TITULO_PUNTOS"));
    	
	}
	
    //**************************************************************************************************************************************
    //
    //									V O L V E R    S I N    B O T O N    B A C K
    //
    //**************************************************************************************************************************************

	/**
	 * Si se pulsa el boton back no se hace nada.
	 */
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			Log.d(this.getClass().getName(), "back button pressed");
		}
		
		return false;
	}	
	
}
