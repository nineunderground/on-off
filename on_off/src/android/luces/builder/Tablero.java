/**
On-Off Android app. Basic game where you must switch off all lights
    Copyright (C) 2010  nineunderground@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package android.luces.builder;

import java.util.StringTokenizer;

public class Tablero {

	// Common properties
	public	 boolean 			todasApagadas		=	false;
	public 	int[][]				arrayTablero		=	new int[5][5];
	public 	int[][]				arrayTableroPrevio	=	new int[5][5];
	public	int					nivelActual			=	0;
	public	static	String		bombillaCoordenadas	=	"";
	public	boolean				juegoEnPausa		=	false;
	public	int					points				=	0;
	
	// Constructor
	public Tablero(String []listaNiveles, int nivelInicial){
		
		// El primer nivel es el 0 para el movil, 1 para el usuario
		// this.nivelActual = 0;
		this.nivelActual = nivelInicial;
		
		// Cuando se crea la instancia, se carga el primer nivel
		this.cargarNivel(arrayTablero, listaNiveles[this.nivelActual]);
		
	}
	
    /**
     * @param arrayTablero
     * Se inicializa el tablero con las posiciones de las dos dimensiones del tablero
     * No es necesario inicializar la tercera dimension que tendra 0 por defecto. 
     * Luego se inicializa esa tercera dimension, en funcion del nivel de juego.
     */
	public void cargarNivel(int[][] arrayTablero, String nivel) {
        
        StringTokenizer	tokenCasilla	=	new StringTokenizer(nivel,";");
        StringTokenizer tokencoordenada;	

        int	coor_horizontal	=	0;
        int	coor_vertical	=	0;
        
        // Mientras haya coordenadas que vienen en el string: nivel
        while (tokenCasilla.hasMoreTokens()){
     
            tokencoordenada	=	new StringTokenizer( tokenCasilla.nextToken() ,":");
            
            coor_horizontal	=	Integer.parseInt(tokencoordenada.nextToken());
            coor_vertical	=	Integer.parseInt(tokencoordenada.nextToken());
            
            //System.out.println("PONEMOS LUZ EN LA CASILLA: " + coor_horizontal + ":" + coor_vertical + "\n");
            
            arrayTablero[coor_horizontal][coor_vertical]	=	1;
            
        }

    }
	
    /**
     * Cada vez que se pulsa un interruptor se cambian las imagenes que procedan..
     * @param arrayTablero
     */
    public void cambiarLuces(int posHorizontal, int posVertical) {
        // TODO Auto-generated method stub
        
        //Se cambia el valor de la posicion actual
        if( this.arrayTablero[posHorizontal][posVertical] == 1 ){
            
        	this.arrayTablero[posHorizontal][posVertical] = 0;
            
        }else{
            
        	this.arrayTablero[posHorizontal][posVertical] = 1;
            
        }
        
        //Se cambia el valor del superior
        
        // Se comprueba que no se pase del borde...
        if( posHorizontal  != 0 ){
        
	        if( this.arrayTablero[posHorizontal - 1][posVertical] == 1 ){
	            
	        	this.arrayTablero[posHorizontal - 1][posVertical] = 0;
	            
	        }else{
	            
	        	this.arrayTablero[posHorizontal - 1][posVertical] = 1;
	            
	        }
        }
        
        // Se cambia el valor del inferior
        if( posHorizontal  != 4 ){
            
	        if( this.arrayTablero[posHorizontal + 1][posVertical] == 1 ){
	            
	        	this.arrayTablero[posHorizontal + 1][posVertical] = 0;
	            
	        }else{
	            
	        	this.arrayTablero[posHorizontal + 1][posVertical] = 1;
	            
	        }
	        
        }
        // Se cambia el valor del izquierdo
	    if( posVertical  != 0 ){
	        
	        if( this.arrayTablero[posHorizontal][posVertical - 1] == 1 ){
	            
	        	this.arrayTablero[posHorizontal][posVertical - 1] = 0;
	            
	        }else{
	            
	        	this.arrayTablero[posHorizontal][posVertical - 1] = 1;
	            
	        }
	        
	    }
        // Se cambia el valor del derecho
	    if( posVertical  != 4 ){
	        
	        if( this.arrayTablero[posHorizontal][posVertical + 1] == 1 ){
	            
	        	this.arrayTablero[posHorizontal][posVertical + 1] = 0;
	            
	        }else{
	            
	        	this.arrayTablero[posHorizontal][posVertical + 1] = 1;
	            
	        }
	        
	    }
    }
	
}