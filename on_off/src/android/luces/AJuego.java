/**
On-Off Android app. Basic game where you must switch off all lights
    Copyright (C) 2010  nineunderground@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package android.luces;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnKeyListener;
import android.graphics.Typeface;
import android.luces.builder.Tablero;
import android.luces.utils.GlobalValues;
import android.luces.utils.Navigation;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.InputFilter;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TextView;

public class AJuego extends Activity {

	// Common properties
	public 	Tablero panelLuces;
	public	List<Button> listaBotones = new ArrayList<Button>(25);
	public	MediaPlayer lightSoundOn, lightSoundOff, levelCompleteSound, gameFinish;
	public 	String		[]arrayNiveles;
	public 	String		[]arrayNivelesInicial;
	Random generator = new Random();
	int 	position = 0;
	int		contadorPantalla	=	0;
	
	public int	timeGame	=	0;
	public boolean finPantalla = false;
	private int limiteTime	=	0;
	
	Thread	thr;
	Handler mHandler;
	public boolean threadLaunch	=	false;
	
	static AlertDialog alert;
	
	Bundle savedBundle;
	
	/**
	 * Se crea el tablero del juego
	 */
	public void onCreate(Bundle savedInstanceState) {
    	
		super.onCreate(savedInstanceState);
		
		if(savedInstanceState == null){
			
			// Quitamos la barra de titulo
	        requestWindowFeature(Window.FEATURE_NO_TITLE);
	        
	        // Sin barra de notificaciones
	        Navigation.setFullScreenMode(this);
	        
	        setContentView(R.layout.tablero);
			
	        /*
			arrayNiveles	=	getResources().getStringArray(R.array.listaNivelesDeJuego);
			// Se carga el nivel que tiene arrayNiveles
			panelLuces		=	new Tablero(arrayNiveles);
			*/
	        
			//*****************************************************************************************************************************************************************
			//Log.d("PRUEBA", "VAMOS A CARGAR OTRO ARRAY CON LOS NIVELES DE JUEGO\n");
			
			arrayNivelesInicial	=	getResources().getStringArray(R.array.listaNivelesDeJuego);
			
			position	=	generator.nextInt(arrayNivelesInicial.length);
			
			//Log.d("PRUEBA", "DAME UNA POSICION DE 1 A " + (arrayNivelesInicial.length - contadorPantalla) + " --> "  + (position) + "\n");
			
			panelLuces	=	new Tablero(arrayNivelesInicial, position);
			
			//Log.d(this.getClass().getName(), "DAME UNA POSICION DE 1 A " + (arrayNivelesInicial.length - contadorPantalla) + " --> "  + (position) + "\n");
			//Log.d("PRUEBA", "EL PRIMER NIVEL QUE TOCA ALEATORIO ES: "  + arrayNivelesInicial[position] + "\n");
			/*
			// Si posicion no es el ultimo valor... Ponemos el ultimo valor en la posicion position
			if( position != (arrayNivelesInicial.length - panelLuces.nivelActual) ){
				
				arrayNivelesInicial[position - 1] = arrayNivelesInicial[arrayNivelesInicial.length - 1];
				panelLuces.nivelActual++;
				
			}
			
			position	=	generator.nextInt(arrayNivelesInicial.length - panelLuces.nivelActual);
			
			Log.d(this.getClass().getName(), "DAME UNA POSICION DE 1 A " + (arrayNivelesInicial.length - panelLuces.nivelActual) + " --> "  + position + "\n");
			Log.d(this.getClass().getName(), "EL SEGUNDO NIVEL QUE TOCA ALEATORIO ES:"  + arrayNivelesInicial[position - 1] + "\n");
			*/
			//*****************************************************************************************************************************************************************
			
			//Se cargan los eventos de las bombillas que se pueden pulsar
			cargarArrayBotones();
			eventosLuces();
			situarLuces(panelLuces.arrayTablero);
			
			// Se establecen parametros de inicio para las puntuaciones, niveles y tiempo
			resetLevelTag();
			
			limiteTime		=	limiteTiempo(panelLuces.nivelActual);
			
	        // Elementos multimedia
	        
	        lightSoundOn		=	MediaPlayer.create(getBaseContext(), R.raw.switch_on);
	        lightSoundOff		=	MediaPlayer.create(getBaseContext(), R.raw.switch_off);
	        levelCompleteSound	=	MediaPlayer.create(getBaseContext(), R.raw.claps);
	        gameFinish			=	MediaPlayer.create(getBaseContext(), R.raw.fin_juego);
	        
	        //savedBundle = savedInstanceState;
	        
	        // Manejador de acciones del hilo de tiempo
	        mHandler = new Handler() {
	        	
	        	@Override
	            public void handleMessage(Message msg) {
	    			
	    			switch (msg.what){
	    				
		    			case GlobalValues.MESSAGE_FIN_TIEMPO:
		    			
		    				//Log.d(this.getClass().getName(), "MENSAGE CAPTURADO DESPUES DE PARAR EL HILO\n");
		    				
		    				// Ponemos la interfaz de fin de Juego
		    				
		    				showDialog(GlobalValues.ALERT_INTRODUCIR_PUNTOS);
		    			
		    			case GlobalValues.MESSAGE_CHANGE_SECONDS:
		    			
			    			TextView	marcador		=	(TextView)findViewById(R.id.marcador);
		                	
		                	String		textTimeSeconds		=	(String) marcador.getText();
		                	//textTimeSeconds 	=	textTimeSeconds.replaceAll( textTimeSeconds , "TIEMPO: ".concat(String.valueOf(timeGame)) );
		                	textTimeSeconds 	=	textTimeSeconds.replaceAll( textTimeSeconds , Navigation.traducir(ALuces.getDiccionario(ALuces.language), "TIME").concat(" ").concat(String.valueOf(timeGame)) );
		                	
		                	//Log.d(this.getClass().getName(), "textTimeSeconds = " + textTimeSeconds + "\n");
		                	marcador.setText(textTimeSeconds);
			                
	    			}
	    			
	    		}
	        	
	        };

	        //Log.d(this.getClass().getName(), "LLAMADA AL HILO\n");
	        startTime();
	        
	        setContentLanguage();
	        
		}
			
    }
	
	public void onDestroy(){
		
		super.onDestroy();
		
		if(lightSoundOn != null){
			
			lightSoundOn.release();
			
		}
		
		if(lightSoundOff != null){
			
			lightSoundOff.release();
			
		}

		if(levelCompleteSound != null){
	
			levelCompleteSound.release();
	
		}

		if(gameFinish != null){
			
			gameFinish.release();
			
		}
        
	}
	
	/**
	 * Se establece el panel en la pantalla con las luces pertinentes encendidas, despues de la pausa
	 */
	public void ponerPanel(){
		
		setContentView(R.layout.tablero);
		
		cargarArrayBotones();
		
		situarLuces(panelLuces.arrayTableroPrevio);
		
		//Se cargan los eventos de las bombillas que se pueden pulsar
        eventosLuces();
		
	}
	
	/**
	 * Metemos todos los botones en nombreuna coleccion de objetos
	 */
	private void cargarArrayBotones() {
		
		TableLayout marco_bombi = (TableLayout)findViewById(R.id.marco_de_bombillas);
		
    	Button		botonBombi_00	=	(Button) marco_bombi.findViewById(R.id.boton_luz_00);
    	Button		botonBombi_01	=	(Button) marco_bombi.findViewById(R.id.boton_luz_01);
    	Button		botonBombi_02	=	(Button) marco_bombi.findViewById(R.id.boton_luz_02);
    	Button		botonBombi_03	=	(Button) marco_bombi.findViewById(R.id.boton_luz_03);
    	Button		botonBombi_04	=	(Button) marco_bombi.findViewById(R.id.boton_luz_04);
    	
    	Button		botonBombi_10	=	(Button) marco_bombi.findViewById(R.id.boton_luz_10);
    	Button		botonBombi_11	=	(Button) marco_bombi.findViewById(R.id.boton_luz_11);
    	Button		botonBombi_12	=	(Button) marco_bombi.findViewById(R.id.boton_luz_12);
    	Button		botonBombi_13	=	(Button) marco_bombi.findViewById(R.id.boton_luz_13);
    	Button		botonBombi_14	=	(Button) marco_bombi.findViewById(R.id.boton_luz_14);
    	
    	Button		botonBombi_20	=	(Button) marco_bombi.findViewById(R.id.boton_luz_20);
    	Button		botonBombi_21	=	(Button) marco_bombi.findViewById(R.id.boton_luz_21);
    	Button		botonBombi_22	=	(Button) marco_bombi.findViewById(R.id.boton_luz_22);
    	Button		botonBombi_23	=	(Button) marco_bombi.findViewById(R.id.boton_luz_23);
    	Button		botonBombi_24	=	(Button) marco_bombi.findViewById(R.id.boton_luz_24);
    	
    	Button		botonBombi_30	=	(Button) marco_bombi.findViewById(R.id.boton_luz_30);
    	Button		botonBombi_31	=	(Button) marco_bombi.findViewById(R.id.boton_luz_31);
    	Button		botonBombi_32	=	(Button) marco_bombi.findViewById(R.id.boton_luz_32);
    	Button		botonBombi_33	=	(Button) marco_bombi.findViewById(R.id.boton_luz_33);
    	Button		botonBombi_34	=	(Button) marco_bombi.findViewById(R.id.boton_luz_34);
    	
    	Button		botonBombi_40	=	(Button) marco_bombi.findViewById(R.id.boton_luz_40);
    	Button		botonBombi_41	=	(Button) marco_bombi.findViewById(R.id.boton_luz_41);
    	Button		botonBombi_42	=	(Button) marco_bombi.findViewById(R.id.boton_luz_42);
    	Button		botonBombi_43	=	(Button) marco_bombi.findViewById(R.id.boton_luz_43);
    	Button		botonBombi_44	=	(Button) marco_bombi.findViewById(R.id.boton_luz_44);
    	
    	listaBotones.add(botonBombi_00);
    	listaBotones.add(botonBombi_01);
    	listaBotones.add(botonBombi_02);
    	listaBotones.add(botonBombi_03);
    	listaBotones.add(botonBombi_04);
    	
    	listaBotones.add(botonBombi_10);
    	listaBotones.add(botonBombi_11);
    	listaBotones.add(botonBombi_12);
    	listaBotones.add(botonBombi_13);
    	listaBotones.add(botonBombi_14);
    	
    	listaBotones.add(botonBombi_20);
    	listaBotones.add(botonBombi_21);
    	listaBotones.add(botonBombi_22);
    	listaBotones.add(botonBombi_23);
    	listaBotones.add(botonBombi_24);
    	
    	listaBotones.add(botonBombi_30);
    	listaBotones.add(botonBombi_31);
    	listaBotones.add(botonBombi_32);
    	listaBotones.add(botonBombi_33);
    	listaBotones.add(botonBombi_34);
    	
    	listaBotones.add(botonBombi_40);
    	listaBotones.add(botonBombi_41);
    	listaBotones.add(botonBombi_42);
    	listaBotones.add(botonBombi_43);
    	listaBotones.add(botonBombi_44);
    	
		
	}


	/**
	 * Se cargan los eventos para las luces que recibiran la pulsacion para encenderse/apagarse
	 */
    private void eventosLuces() {
		
    	// Se cargan eventos a todos los botones
    	for(int cont=0; cont<listaBotones.size(); cont++ ){
    		
    		TableLayout marco_bombi = (TableLayout)findViewById(R.id.marco_de_bombillas);
    		
    		Button	botonBombi	=	(Button)listaBotones.get(cont);
    		botonBombi			=	(Button)marco_bombi.findViewById(botonBombi.getId());
    		
    		botonBombi.setOnTouchListener( new OnTouchListener() {
    			
    			public boolean onTouch(View arg0, MotionEvent arg1) {
    				
    				if ( arg1.getAction() == MotionEvent.ACTION_DOWN ) {
    					
    					Button bombi	=	(Button)arg0;
    					StringTokenizer tagTokens	=	new StringTokenizer((String)bombi.getTag(),":");
    					
    					pulsarLuz(	Integer.parseInt(tagTokens.nextToken()), // Row     X
    								Integer.parseInt(tagTokens.nextToken()), // Colummn Y
    								panelLuces);
    					
    					return true;
    				}
    				
    				return false;
    				
    			}
    			
    		});
    		
    	}
    		
	}
    
    /**
     * @param arrayTablero, int x, int y
     * 
     */
    public void pulsarLuz(int x, int y, Tablero panelLuces) {
        
    	panelLuces.cambiarLuces(x, y);
        
    	if( panelLuces.arrayTablero[x][y] == 1 ){
    		playLightOn();
    	}else{
    		playLightOff();
    	}
    	
    	this.situarLuces(panelLuces.arrayTablero);
        
        if( panelLuces.todasApagadas == true ){
            
            // Fin de pantalla
        	finPantalla = true; // Con este valor paramos el hilo
        	
            //System.out.println("NIVEL SUPERADO\n");
            //panelLuces.nivelActual++;
            
            // Se calcula de nuevo el limite de tiempo
        	limiteTime		=	limiteTiempo(panelLuces.nivelActual);
            
            playLevelComplete();
            
            // Si todavia hay mas niveles de juego, se carga el siguiente.
            //if( panelLuces.nivelActual < arrayNiveles.length ){
            if( (contadorPantalla + 1) < arrayNivelesInicial.length ){
            	
            	threadLaunch	=	false;
            	showDialog(GlobalValues.ALERT_NEXT_LEVEL);
            	
            	// Se carga el siguiente nivel
            	//panelLuces.cargarNivel(panelLuces.arrayTablero, arrayNiveles[panelLuces.nivelActual]);
            	
            	//******************************************************************************************************************
            	// Si posicion no es el ultimo valor... Ponemos el ultimo valor en la posicion position
            	
            	/*
            	panelLuces	=	new Tablero(arrayNivelesInicial, position - 1);
			
				Log.d(this.getClass().getName(), "DAME UNA POSICION DE 1 A " + (arrayNivelesInicial.length - numeroPantalla) + " --> "  + (position) + "\n");
				Log.d(this.getClass().getName(), "EL PRIMER NIVEL QUE TOCA ALEATORIO ES:"  + arrayNivelesInicial[position - 1] + "\n");
            	*/
            	
    			if( (arrayNivelesInicial.length - contadorPantalla) > 2){
    				// Last stage
    				contadorPantalla++;
    				if(position == arrayNivelesInicial.length - contadorPantalla){
    					
    				}else{
    					arrayNivelesInicial[position] = arrayNivelesInicial[arrayNivelesInicial.length - contadorPantalla];
    				}
    				
    				// Si el que ha salido es la ultima posicion...
    				/*
    				if( arrayNivelesInicial[position].equals(arrayNivelesInicial[arrayNivelesInicial.length - contadorPantalla]) ){
    					arrayNivelesInicial[position] = arrayNivelesInicial[arrayNivelesInicial.length - contadorPantalla];
    				}
    				*/
    				position				=	generator.nextInt(arrayNivelesInicial.length - contadorPantalla);
    			}else{
    				
    				contadorPantalla++;
    				if(position > 0){
    					position				=	0;
    				}else{
    					position				=	1;
    				}
    				
    			}
    			
    			panelLuces.nivelActual 	=	position;
    			panelLuces.cargarNivel(panelLuces.arrayTablero, arrayNivelesInicial[panelLuces.nivelActual]);
    			
    			//Log.d(this.getClass().getName(), "DAME UNA POSICION DE 1 A " + (arrayNivelesInicial.length - contadorPantalla) + " --> "  + position + "\n");
    			//Log.d("PRUEBA", "EL SIGUIENTE NIVEL QUE TOCA ALEATORIO ES: "  + arrayNivelesInicial[position] + "\n");
    			
    			//******************************************************************************************************************
            	
            	panelLuces.todasApagadas	=	false;
            	
            }else{
            	
            	//	Se recalculan los puntos
	    		panelLuces.points	=	(500 - timeGame) + panelLuces.points;
            	
            	//System.out.println("TODOS LOS NIVELES SUPERADOS\n");
            	showDialog(GlobalValues.ALERT_INTRODUCIR_PUNTOS);
            	
            }
            
        }
        
    }

    /**
     * @param arrayTablero
     * Se encienden o apagan las luces que procedan
     */
    public void situarLuces(int[][] arrayLuces) {
        
    	panelLuces.todasApagadas	=	true;
    	
        // Se recorre el tablero y se ponen las luces correspondientes encendidas
        for(int cont =0; cont < arrayLuces.length ; cont++){
            
            // for para la segunda dimension
            for(int cont2 =0; cont2 < arrayLuces.length ; cont2++){
             
                if( arrayLuces[cont][cont2] ==	1){
                    
                	ponerBombillaEncendida(cont, cont2); 
                	//Tablero.bombillaCoordenadas	= String.valueOf(cont) + ":" +  String.valueOf(cont2);	
                	
                	panelLuces.todasApagadas =	false;
                    //System.out.println("HAY QUE PONER LA IMAGEN DE ESA CASILLA CON LA LUZ ENCENDIDA: \n");
                    
                }else{
                    
                	this.ponerBombillaApagada(cont, cont2);
                    //System.out.println("HAY QUE PONER LA IMAGEN DE ESA CASILLA CON LA LUZ APAGADA: \n");
                    
                }
                
            }   
            
        }
    }
    
    /**
     * Pone la imagen del boton con la imagen de la bombilla encendida
     * @param x
     * @param y
     */
    public void ponerBombillaEncendida(int x, int y){
    	
    	String coordenada	=	String.valueOf(x).concat(":").concat(String.valueOf(y));
    	
    	TableLayout marco_bombi = (TableLayout)findViewById(R.id.marco_de_bombillas);
		
    	// Se recorren todos los botones y se pone la imagen encendida en ese boton
    	for(int cont=0; cont<listaBotones.size(); cont++ ){
    	
    		Button		botonBombi	=	(Button) this.listaBotones.get(cont);
    		
    		if( botonBombi.getTag().equals(coordenada) ){
    			// Se vuelve a recuperar el boton, porque parece que le gusta mas recuperarlo del layout
    			botonBombi 	=	(Button)marco_bombi.findViewById(botonBombi.getId());
    			
    			botonBombi.setBackgroundResource(R.drawable.bombi_encendida_fondo);
    			break;
    			
    		}
    		
    	}
    	
    }

    /**
     * Pone la imagen del boton con la imagen de la bombilla apagada
     * @param x
     * @param y
     */
    public void ponerBombillaApagada(int x, int y){
    	
    	String coordenada	=	String.valueOf(x).concat(":").concat(String.valueOf(y));
    	
    	TableLayout marco_bombi = (TableLayout)findViewById(R.id.marco_de_bombillas);
    	
    	// Se recorren todos los botones y se pone la imagen encendida en ese boton
    	for(int cont=0; cont<listaBotones.size(); cont++ ){
    	
    		Button		botonBombi	=	(Button) this.listaBotones.get(cont);
    		
    		if( botonBombi.getTag().equals(coordenada) ){
    			// Se vuelve a recuperar el boton, porque parece que le gusta mas recuperarlo del layout
    			botonBombi 	=	(Button)marco_bombi.findViewById(botonBombi.getId());

    			botonBombi.setBackgroundResource(R.drawable.bombi_apagada_fondo);
    			break;
    			
    		}
    		
    	}
    	
    }
    
    //**************************************************************************************************************************************
    //
    //									M E N U
    //
    //**************************************************************************************************************************************
    
    /**
     * Menu de opciones de la pantalla principal
     * 	-	VOLVER		- Volver a la pantalla principal
     * -	PAUSA		- Pausar el juego
     * -	COMENTARIO	- Contiene un comentario de cada foto
     * @return 
     */
    public boolean onCreateOptionsMenu(Menu menu){
		
    	super.onCreateOptionsMenu(menu);
        
    	menu.setQwertyMode(true);
    	
    	MenuItem mnu1 = menu.add(0, GlobalValues.MENU_TERMINAR_PARTIDA, 0, Navigation.traducir(ALuces.getDiccionario(ALuces.language), "MENU_TERMINAR"));
        {
            mnu1.setAlphabeticShortcut('q');
            mnu1.setIcon(R.drawable.go_back);            
        }
        
        MenuItem mnu2 = menu.add(0, GlobalValues.MENU_PAUSAR_PARTIDA, 1, Navigation.traducir(ALuces.getDiccionario(ALuces.language), "MENU_PAUSAR"));
        {
            mnu2.setAlphabeticShortcut('p');
            mnu2.setIcon(R.drawable.icono_musica_pausa);            
        }
        
        MenuItem mnu3 = menu.add(0, GlobalValues.MENU_MUSICA_JUEGO, 1, Navigation.traducir(ALuces.getDiccionario(ALuces.language), "MENU_SONIDO"));
        {
            mnu3.setAlphabeticShortcut('s');
            
            if (!ALuces.gameSound){
            	mnu3.setIcon(R.drawable.sound_switch_icon_no);
            }else{
            	mnu3.setIcon(R.drawable.sound_switch_icon);
            }
                        
        }
        
        MenuItem mnu4 = menu.add(0, GlobalValues.MENU_RESET_LEVEL, 1, Navigation.traducir(ALuces.getDiccionario(ALuces.language), "MENU_REINICIAR"));
        {
        	mnu4.setAlphabeticShortcut('r');
        	mnu4.setIcon(R.drawable.reset);
//            if (!ALuces.gameSound){
//            	mnu3.setIcon(R.drawable.sound_switch_icon_no);
//            }else{
//            	mnu3.setIcon(R.drawable.sound_switch_icon);
//            }
                        
        }
        
        return true;
        
	}
    
    /**
     * Definicion del comportamiento de los botones de menu principal
     */
    public boolean onOptionsItemSelected(MenuItem item) {
        
    	switch (item.getItemId()) {
    	
            case GlobalValues.MENU_TERMINAR_PARTIDA:
                // Se Abre un popup Dialog preguntando si se quiere volver y que se perderan los cambios
            	showDialog(GlobalValues.ALERT_TERMINAR_PARTIDA);
				
            	return true;
            
            case GlobalValues.MENU_PAUSAR_PARTIDA:
            	
            	// Si el juego no esta ya detenido, entonces se pausa la pantalla de juego
            	if (!panelLuces.juegoEnPausa ){
            		
            		panelLuces.juegoEnPausa	=	true;
            		setContentView(R.layout.pausa);
            		
            		TextView	pauseText	=	(TextView)findViewById(R.id.mensaje_pausa);
            		pauseText.setText(Navigation.traducir(ALuces.getDiccionario(ALuces.language), "PAUSE_TEXT"));
            		
            		// Cambiamos el icono de pausa
            		item.setIcon(R.drawable.icono_musica_pausa_pulsado);
            		
            		// Guardamos la posicion de las luces para despues de la pausa recuperar las bombillas que estaban encendidas
            		panelLuces.arrayTableroPrevio	=	null;
            		panelLuces.arrayTableroPrevio	=	new int[5][5];
            		panelLuces.arrayTableroPrevio	=	this.panelLuces.arrayTablero;
            		
            		return true;
            		
            	}else{
            		
            		panelLuces.juegoEnPausa	=	false;
            		
            		// Cambiamos el icono de pausa
            		item.setIcon(R.drawable.icono_musica_pausa);
            		ponerPanel();
            		resetLevelTag();
            		
            		return true;
            		
            	}
            	
            case GlobalValues.MENU_MUSICA_JUEGO:
            	
            	if(ALuces.gameSound){
            		
            		ALuces.gameSound = false;
					item.setIcon(R.drawable.sound_switch_icon_no);
					
            	}else{
            		
            		ALuces.gameSound = true;
            		item.setIcon(R.drawable.sound_switch_icon);
            		
            	}
            	
            	return true;
            	
            case GlobalValues.MENU_RESET_LEVEL:
            	
            	panelLuces.juegoEnPausa	=	false;
        		
            	// Se penaliza con 300 puntos menos el reiniciar la pantalla:
        		if( panelLuces.points >= 300 ){
        			
        			panelLuces.points	= panelLuces.points - 300;
        			
        		}
            	
        		// Cambiamos el icono de pausa
        		//item.setIcon(R.drawable.icono_musica_pausa);
            	setContentView(R.layout.tablero);
        		cargarArrayBotones();
        		
        		panelLuces.arrayTablero	=	null;
        		panelLuces.arrayTablero	=	new int[5][5];
        		panelLuces.cargarNivel(panelLuces.arrayTablero, arrayNivelesInicial[panelLuces.nivelActual]);
        		
        		panelLuces.todasApagadas	=	false;
        		situarLuces(panelLuces.arrayTablero);
        		eventosLuces();
            	
        		resetLevelTag();
        		
//        		TextView 	currentLevel	=	(TextView)findViewById(R.id.nivel_id);
//        		String		numLevel		=	"NIVEL ";
//        		
//        		numLevel	=	numLevel.concat( String.valueOf(panelLuces.nivelActual + 1) );
//        		currentLevel.setText(numLevel);
//
//        		TextView 	currentPoints	=	(TextView)findViewById(R.id.puntos_juego);
//        		String		points			=	(String)currentPoints.getText();
//        		
//        		points	=	"PUNTOS ACUMULADOS: ".concat( String.valueOf(panelLuces.points) );
//        		currentPoints.setText(points);
        		
        		return true;
        		
        }

        return false;
    }

    //**************************************************************************************************************************************
    //
    //									A L E R T A S    C O N T R O L A D A S
    //
    //**************************************************************************************************************************************
    
    /**
	 * Muestra la alerta de confirmacion de cierre de la aplicacion
	 */
	protected AlertDialog onCreateDialog(int id) {
        
		//AlertDialog.Builder builder = new AlertDialog.Builder(this);
		//AlertDialog.Builder builder = null;
		
		alert	=	null;
		
		switch(id) {
	        
			case GlobalValues.ALERT_TERMINAR_PARTIDA:
	            
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				
				builder.setTitle(Navigation.traducir(ALuces.getDiccionario(ALuces.language), "SALIR_JUEGO"));
				builder.setIcon(R.drawable.bombi_encendida);
				
				builder.setMessage(Navigation.traducir(ALuces.getDiccionario(ALuces.language), "SALIR_MENSAJE"));
				
			    builder.setPositiveButton(Navigation.traducir(ALuces.getDiccionario(ALuces.language), "Si"), new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int id) {
			        	//Log.d(this.getClass().getName(), "FIN DE JUEGO \n");
			        	finPantalla = true;
			        	AJuego.this.finish();
			        }
			    });
			    
			    builder.setNegativeButton(Navigation.traducir(ALuces.getDiccionario(ALuces.language), "No"), new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int id) {
			        	//Log.d(this.getClass().getName(), "SIGUE EL JUEGO COMO SI NADA \n");
			        }
			    });
				
			    alert = builder.create();
	            
			    alert.setOnKeyListener( new OnKeyListener() {
					
					public boolean onKey(DialogInterface arg0, int arg1, KeyEvent arg2) {
						return true;		
					}
				
			    });
			    
	            break;
	        
			case GlobalValues.ALERT_NEXT_LEVEL:
	            
				AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
				
				builder2.setTitle(Navigation.traducir(ALuces.getDiccionario(ALuces.language), "ENHORABUENA"));
				builder2.setIcon(R.drawable.thumbs_up_icon);
				
				builder2.setMessage(Navigation.traducir(ALuces.getDiccionario(ALuces.language), "LEVEL_COMPLETE"));
				
			    builder2.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int id) {
			        	
			        	if(!threadLaunch){
			        	
				        	situarLuces(panelLuces.arrayTablero);
				        	
				        	resetLevelTag();
				        	
				        	//	Se recalculan los puntos
				    		panelLuces.points	=	(500 - timeGame) + panelLuces.points;
				        	
				    		TextView 	currentPoints	=	(TextView)findViewById(R.id.puntos_juego);
				    		String		points			=	(String)currentPoints.getText();
				    		
				    		//points	=	"PUNTOS ACUMULADOS: ".concat( String.valueOf(panelLuces.points) );
				    		points	=	Navigation.traducir(ALuces.getDiccionario(ALuces.language), "TOTAL_SCORE").concat(" ").concat( String.valueOf(panelLuces.points) );
				    		
				    		currentPoints.setText(points);
				    		
				        	if (ALuces.gameSound){
				        		
				        		if(levelCompleteSound != null && levelCompleteSound.isPlaying() ){
				        			levelCompleteSound.stop();
				        			levelCompleteSound.release();
				        			//Log.d("PRUEBA", "PARAMOS LA MUSICA DE EXITO\n");
				        		}else{
				        			//Log.d("PRUEBA", " NO PARAMOS LA MUSICA DE EXITO\n");
				        			levelCompleteSound.release();
				        		}
				        		
				        	}
				        	
				        	// Volvemos a arrancar el tiempo
				        	timeGame	=	0;
				        	finPantalla	=	false;
				        	
				        	Log.d(this.getClass().getName(), "PARAMOS EL HILO ANTES DE LLAMARLO\n");
				        	thr.stop();
				        	
				        	try{
				        		Thread.currentThread().sleep(500);
				        	}catch(InterruptedException err){
				        		
				        	}
				        	//Log.d(this.getClass().getName(), "LLAMAMOS AL HILO DE NUEVO!!\n");
				        	//Log.d(this.getClass().getName(), "thr.isAlive() " + thr.isAlive() + "\n");
				        	
				        	if( !thr.isAlive() ){
				        		startTime();
				        	}
				        	threadLaunch	=	true;
			        	
			        	}
			        	
			        }
			    });
			    
			    alert = builder2.create();
			    
			    alert.setOnKeyListener( new OnKeyListener() {
					
						public boolean onKey(DialogInterface arg0, int arg1, KeyEvent arg2) {
							// TODO Auto-generated method stub
							
							//Log.d(this.getClass().getName(), "Si se pulsa cualquier boton se cancela su accion devolviendo true");
							return true;
							/*
							if ((keyCode == KeyEvent.KEYCODE_BACK)) {
				    			Log.d(this.getClass().getName(), "Back button pressed");
				    		}
							*/
							
						}
					
				});
			    
	            break;
	        
			case GlobalValues.ALERT_INTRODUCIR_PUNTOS:
	            
				if (ALuces.gameSound){
		        	
					if(gameFinish != null){
						gameFinish.start();
					}
					
	        	}
				
				AlertDialog.Builder builder3 = new AlertDialog.Builder(this);
				
				if((panelLuces.points == 0) || !ALuces.gameScores.inHighscore(panelLuces.points)){
					
					String masPuntos	=	Navigation.traducir(ALuces.getDiccionario(ALuces.language), "GET_MORE_POINTS");
					
					builder3.setTitle(Navigation.traducir(ALuces.getDiccionario(ALuces.language), "END_GAME"));
					
					builder3.setMessage(masPuntos);
					
					builder3.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				        
						public void onClick(DialogInterface dialog, int id) {
				        	
							AJuego.this.finish();
				        		
				        }
						
				    });
					
				}else{
					
					builder3.setTitle( String.valueOf(panelLuces.points).concat(" PUNTOS") );
					String endMsg	=	"";
					
					if( (contadorPantalla + 1) == arrayNivelesInicial.length ){
						
						//endMsg	=	getString(R.string.descripcion_juego_entero_superado);
						endMsg		=	Navigation.traducir(ALuces.getDiccionario(ALuces.language), "ALL_LEVELS_CLEAR");
						
					}else{
						
						//endMsg	=	getString(R.string.descripcion_introducir_nombre);
						endMsg		=	Navigation.traducir(ALuces.getDiccionario(ALuces.language), "TIME_OVER");
						
					}
					
					builder3.setMessage(endMsg);
					
					final EditText	name 	=	new EditText(getBaseContext());
					
					// 8 Characteres max.
					InputFilter[]	filtro	=	new InputFilter[1];
					filtro[0]	=	new InputFilter.LengthFilter(7);
					name.setFilters(filtro);
					name.setTypeface(Typeface.SERIF);
					name.requestFocus();
			        final InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
			        //imm.showSoftInput(name,  InputMethodManager.SHOW_IMPLICIT);
			        //imm.hideSoftInputFromInputMethod( alert.getContext() );
			        
					builder3.setView(name);
					
					builder3.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				        
						public void onClick(DialogInterface dialog, int id) {
							// Se guarda nombre y puntutacion
							ALuces.gameScores.addScore( String.valueOf( name.getText() ) , panelLuces.points);
							imm.hideSoftInputFromWindow( name.getWindowToken(), 0 );
							
							Intent currentIntent	= getIntent();
							currentIntent.putExtra("last_score", ALuces.gameScores.getLastPosition() );
							//AJuego.this.finishActivity(GlobalValues.ACTIVITY_CODE_GAME);
							setResult(RESULT_OK, currentIntent);
							AJuego.this.finish();
								
				        }
						
				    });
					
				}
				
				/*
				try{
					Thread.currentThread().sleep(1000);
				}catch(InterruptedException err){
					
				}
				*/
				AJuego.alert = builder3.create();
				
				alert.setOnKeyListener( new OnKeyListener() {
					
					public boolean onKey(DialogInterface arg0, int arg1, KeyEvent arg2) {
						
						Log.d("boton pulsado", "arg1: " + arg1 + "/n");
						Log.d("boton pulsado", "arg2.getAction(): " + arg2.getAction() + "/n");
						Log.d("boton pulsado", "arg2.getKeyCode(): " + arg2.getKeyCode() + "/n");
						Log.d("boton pulsado", "arg2.getCharacters(): " + arg2.getCharacters() + "/n");
						
						if (( arg1 == KeyEvent.KEYCODE_BACK) || 
							( arg1 == KeyEvent.KEYCODE_ENTER)
							){
							return true;
						}else{
							return false;
						}
						
					}
					
			    });
				
				break;
				
	        default:
	            
	        	alert = null;
	        	
        }

        return alert;
        
    }
    
    //**************************************************************************************************************************************
    //
    //									V O L V E R    S I N    B O T O N    B A C K
    //
    //**************************************************************************************************************************************

	/**
	 * Si se pulsa el boton back no se hace nada.
	 */
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			//Log.d(this.getClass().getName(), "back button pressed");
		}
		
		return false;
	}
	 
	//**************************************************************************************************************************************
	//
	//									S O N I D O S
	//
	// 
	//**************************************************************************************************************************************
	
	/**
	 * Reproduce el sonido de enceder una bombilla
	 */
	public void playLightOn(){
		
		if (ALuces.gameSound){
			//lightSoundOn		=	MediaPlayer.create(getBaseContext(), R.raw.switch_on);
			lightSoundOn.start();
			//lightSoundOn.release();
		}
		
	}
	
	/**
	 * Reproduce el sonido de apagar una bombilla
	 */
	public void playLightOff(){
		
		if (ALuces.gameSound){
			//lightSoundOff		=	MediaPlayer.create(getBaseContext(), R.raw.switch_off);
			lightSoundOff.start();
			//lightSoundOff.release();
		}
		
	}
	
	/**
	 * Reproduce el sonido de nivel superado
	 */
	public void playLevelComplete(){
		
		if (ALuces.gameSound){
			levelCompleteSound	=	MediaPlayer.create(getBaseContext(), R.raw.claps);
			
			if(levelCompleteSound != null){
				levelCompleteSound.start();
				//levelCompleteSound.release();
			}
			
		}
		
	}
	

	//**************************************************************************************************************************************
	//
	//									M A N E J A D O R		 D E		T I E M P O
	//
	// 
	//**************************************************************************************************************************************

	/**
	 * Arranca el marcador de tiempo del juego
	 */
	public void startTime(){
		
		thr = new Thread(new Runnable() {

            public void run() {
            	
            	//Log.d(this.getClass().getName(), "HILO ARRANCADO\n");
            	
            	//Log.d(this.getClass().getName(), "thr.isAlive() " + thr.isAlive() + "\n");
            	
            	while(  !finPantalla && timeGame < limiteTime){
                	
                		try{
                		
                			
//                			mHandler.post(
//                					
//			                		new Runnable() {
//			    	                    public void run() {
//			    	        
//			    	                    	//while( !finPantalla || timeGame < 10){
//			    	                    	
//				    	                    	timeGame++;
//					    	                    mHandler.sendEmptyMessage(GlobalValues.MESSAGE_CHANGE_SECONDS);
//					    	                    
//			    	                    	//}
//			    	                    	/*
//			    	                    	TextView	marcador	=	(TextView)findViewById(R.id.marcador);
//			    	                    	
//			    	                    	String		timeSeconds	=	(String) marcador.getText();
//			    	                    	String numeroSegundos = timeSeconds.replaceFirst("X", String.valueOf( timeGame ) );
//			    	                    	
//			    	                    	marcador.setText(numeroSegundos);
//			    	                    	
//			    	                    	Log.d(this.getClass().getName(), "SE AVANZA UN SEGUNDO, VALOR DE timeGame= " + timeGame + "\n");
//			    	                    	*/
//			    	                    }
//
//			    	                }
//			                
//			                );
                			
                			if (!panelLuces.juegoEnPausa ){
                				
                				timeGame++;
    					    	mHandler.sendEmptyMessage(GlobalValues.MESSAGE_CHANGE_SECONDS); 
                    			 
                    			// En milisegundos 
                    			Thread.currentThread().sleep(1000);
                				
                			}
                			
                		}catch(InterruptedException err){
                			
                			//Log.d(this.getClass().getName(), err.getLocalizedMessage() + " HEMOS PARADO EL HILO \n");
                			mHandler.sendEmptyMessage(GlobalValues.MESSAGE_FIN_TIEMPO);
                			
                		}
                		
                }
            	
            	if(	timeGame >= limiteTime ){
                	
            		//Log.d(this.getClass().getName(), "FIN DEL HILO DE INTERFAZ \n");
                    // Cuando sale del bucle se acaba el hilo y por tanto la partida
            		
                    mHandler.sendEmptyMessage(GlobalValues.MESSAGE_FIN_TIEMPO);
                    //Thread.currentThread().interrupted();
            	}
            	
            	//Log.d(this.getClass().getName(), "HILO TERMINADO\n");
            	
            }
            
            /*
			public void stop() {
            	
            	Log.d(this.getClass().getName(), " HEMOS PARADO EL HILO \n");
            	
            }
            */
        });
    	
    	thr.start();
    	
	}
	
	//**************************************************************************************************************************************
	//
	//									O T R O S 
	//
	// 
	//**************************************************************************************************************************************
	
	/**
	 * Seteando el nivel en el marcador 
	 */
	public void resetLevelTag(){
		
		TextView 	currentLevel	=	(TextView)findViewById(R.id.nivel_id);
		String		numLevel		=	Navigation.traducir(ALuces.getDiccionario(ALuces.language), "NIVEL").concat(" ");
		
		//numLevel	=	numLevel.concat( String.valueOf(panelLuces.nivelActual + 1) );
		numLevel	=	numLevel.concat( String.valueOf(contadorPantalla + 1) );
		
		currentLevel.setText(numLevel);

		TextView 	currentPoints	=	(TextView)findViewById(R.id.puntos_juego);
		String		points			=	(String)currentPoints.getText();
		
		//points	=	"PUNTOS ACUMULADOS: ".concat( String.valueOf(panelLuces.points) );
		points	=	Navigation.traducir(ALuces.getDiccionario(ALuces.language), "TOTAL_SCORE").concat(" ").concat( String.valueOf(panelLuces.points) );
		
		currentPoints.setText(points);
		
	}
	
	/**
	 * 
	 * @return limite de tiempo para el nivel
	 */
	private int limiteTiempo(int level){
		
		int tiempo = 60;
		
		/*
		if(level > -1 && level < 5 ){
			// First 5 levels
			tiempo	=	45;
			
		}else if(level > 4 && level < 10 ){
			// Second 5 levels
			tiempo	=	75;
			
		}else if(level > 9 && level < 15 ){
			// Third 5 levels
			tiempo	=	140;
			
		}else{
			// Rest
			tiempo	=	225;
			
		}
		*/
		return tiempo;
	}
	
	//**************************************************************************************************************************************
    //
    //									T R A D U C T I O N
    //
    //**************************************************************************************************************************************

	/**
     * Sets strings according language supplied to this function
     * 
     */
    private void setContentLanguage() {
		
    	//TextView	nivel	=	(TextView)findViewById(R.id.nivel_id);
    	TextView	tiempo	=	(TextView)findViewById(R.id.marcador);
    	//TextView	puntos	=	(TextView)findViewById(R.id.puntos_juego);
    	
    	//nivel.setText(Navigation.traducir(ALuces.getDiccionario(ALuces.language), "NIVEL"));
    	tiempo.setText(Navigation.traducir(ALuces.getDiccionario(ALuces.language), "TIME"));
    	//puntos.setText(Navigation.traducir(ALuces.getDiccionario(ALuces.language), "TOTAL_SCORE"));
    	
	}
	
}
