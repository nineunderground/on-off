/**
On-Off Android app. Basic game where you must switch off all lights
    Copyright (C) 2010  nineunderground@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package android.luces.utils;

import java.util.StringTokenizer;

import android.app.Activity;
import android.content.Intent;
import android.view.WindowManager;

public class Navigation {

	/**
	 * Set full screen mode
	 * @param act
	 */
	public static void setFullScreenMode(Activity act){
		
		act.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
	}
	
	/**
	 * Navigates between activities
	 * @param activity
	 */
	public static  Intent openActivity(final int activity, Intent myIntent){
		
		switch(activity) {
		
			case GlobalValues.ACTIVITY_CODE_GAME:
			
				myIntent.setClassName(GlobalValues.NAME_MAIN_PACKAGE, GlobalValues.NAME_ACTIVITY_GAME);
				//myIntent.putExtra("android.jatorrak.SpecialValue", "Hello, Joe!"); // key/value pair, where key needs current package prefix.
				break;
	        
			case GlobalValues.ACTIVITY_CODE_HELP:
				
				myIntent.setClassName(GlobalValues.NAME_MAIN_PACKAGE, GlobalValues.NAME_ACTIVITY_HELP);
				break;
			
			case GlobalValues.ACTIVITY_CODE_SCORES:
				
				myIntent.setClassName(GlobalValues.NAME_MAIN_PACKAGE, GlobalValues.NAME_ACTIVITY_SCORES);
				break;	
			
			case GlobalValues.ACTIVITY_CODE_PAUSE:
				
				myIntent.setClassName(GlobalValues.NAME_MAIN_PACKAGE, GlobalValues.NAME_ACTIVITY_PAUSE);
				break;
			
			case GlobalValues.ACTIVITY_CODE_OTHER:
				
				myIntent.setClassName(GlobalValues.NAME_MAIN_PACKAGE, GlobalValues.NAME_ACTIVITY_OTHERS);
				break;
			
		}
			
		return myIntent;
		
	}

	/**
	 * Returns string text traducted. Searches traduction into diccionary supplied
	 * @param dicIngles
	 * @param string
	 * @return
	 */
	public static String traducir(String[] dic, String word) {
		
		StringTokenizer	wordTraducted;
		
		int i = 0;
		
		for(i = 0; i<dic.length ; i++){
			
			if( dic[i].startsWith(word) ){
				
				wordTraducted	=	new StringTokenizer(dic[i], "@/@");
				
				wordTraducted.nextElement();
				word = (String)wordTraducted.nextElement();
				
				break;
				
			}else{
				continue;
			}
			   
		}
		
		if(!(i < dic.length)){
			word	=	"???";
		}
		
		return word;
		
	}

}
