/**
On-Off Android app. Basic game where you must switch off all lights
    Copyright (C) 2010  nineunderground@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package android.luces.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * @author irjimenez
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Highscore {
    
        private SharedPreferences	preferences;
        private String 				names[];
        private long 				score[];
        private int					totalScores;
        private int					lastPosition;
        private static final int	MAX_SCORES = 20; // Numero total de puntuaciones que quieres que tenga el juego
        
        /**
         * Delete all scores
         */
        public void deleteAllScores(Context context){
        	
        	SharedPreferences.Editor editor = preferences.edit();
	        editor.clear();
	        editor.commit();
        	
        }
        
        /**
         * Retrieve last position score added
         * @return
         */
        public int getLastPosition() {
			return lastPosition;
		}

        /**
         * Set last position score added
         * @param lastScore
         */
		public void setLastPosition(int lastScore) {
			this.lastPosition = lastScore;
		}

        public Highscore(Context context){
        	
	        preferences = context.getSharedPreferences("LucesPoints", 0);
	        names = new String[MAX_SCORES];
	        score = new long[MAX_SCORES];
	        totalScores = score.length; 
	
	        for (int x=0; x<MAX_SCORES; x++)
	        {
	        names[x] = preferences.getString("name"+x, "-");
	        score[x] = preferences.getLong("score"+x, 0);
	        }

        }

        /**
         * Return total highscores are stored in memory
         * @return
         */
        public int getScores(){
        	
        	return totalScores;
        }
        
        public String getName(int x){
        //  get the name of the x-th position in the Highscore-List
        	return names[x];
        }

        public long getScore(int x){
        // get the score of the x-th position in the Highscore-List
        	return score[x];
        }

        public boolean inHighscore(long score){
        	// test, if the score is in the Highscore-List
        	
	        int position;
	        for (position=0; position<MAX_SCORES&&this.score[position]>score; 
	        position++);
	
	        if (position==MAX_SCORES) return false;
	        return true;
        }

        public boolean addScore(String name, long score){
        	// add the score with the name to the Highscore-List
        	name	=	name.toLowerCase();
        	
	        int position;
	        
	        for (position=0; position<MAX_SCORES&&this.score[position]>score; position++);
	
	        if (position==MAX_SCORES) return false;
	
	        for (int x=MAX_SCORES-1; x>position; x--){
	        	
		        names[x]=names[x-1];
		        this.score[x]=this.score[x-1];
		        
	        }
	
	        this.names[position] = new String(name);
	        this.score[position] = score;
	
	        this.lastPosition = position;
	        
	        SharedPreferences.Editor editor = preferences.edit();
	        for (int x=0; x<MAX_SCORES; x++){
	        	
		        editor.putString("name"+x, this.names[x]);
		        editor.putLong("score"+x, this.score[x]);
		        
	        }
	        
	        editor.commit();
	        return true;

        }

}
