/**
On-Off Android app. Basic game where you must switch off all lights
    Copyright (C) 2010  nineunderground@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package android.luces.utils;

public final class GlobalValues {

	// Menus options
	public final static int 	MENU_ACERCA_DE			= 0;
	public final static int 	MENU_PAUSAR_PARTIDA		= 1;
	public final static int 	MENU_MUSICA_JUEGO		= 2;
	public final static int 	MENU_RESET_LEVEL		= 3;
	public final static int 	MENU_TERMINAR_PARTIDA	= -1;
	
	// Alertas del juego
	public final static int 	ALERT_EXIT				= 1;
	public final static int 	ALERT_NEXT_LEVEL		= 2;
	public final static int 	ALERT_ABOUT_ME			= 3;
	public final static int 	ALERT_INTRODUCIR_PUNTOS	= 4;
	public final static int 	ALERT_TERMINAR_PARTIDA	= -1;
	
	// Activities
	public final static int 	ACTIVITY_CODE_GAME		= 1;
	public final static int 	ACTIVITY_CODE_HELP		= 2;
	public final static int 	ACTIVITY_CODE_SCORES 	= 3;
	public final static int 	ACTIVITY_CODE_PAUSE 	= 4;
	public final static int 	ACTIVITY_CODE_OTHER 	= 5;
	
	//	Languages codes
	public final static int		FINNISH 		=	0;
	public final static int 	SPANISH 		=	1;
	public final static int 	FRENCH	 		=	2;
	public final static int 	ITALIAN 		=	3;
	public final static int 	GERMAN 			=	4;
	public final static int 	ENGLISH 		=	5;
	
	// Package names
	public final static String 	NAME_MAIN_PACKAGE		= "android.luces";
	public final static String 	NAME_ACTIVITY_START		= "android.luces.ALuces";
	public final static String 	NAME_ACTIVITY_GAME		= "android.luces.AJuego";
	public final static String 	NAME_ACTIVITY_HELP		= "android.luces.AHelp";
	public final static String 	NAME_ACTIVITY_SCORES	= "android.luces.AScores";
	public final static String 	NAME_ACTIVITY_PAUSE		= "android.luces.APause";
	public final static String 	NAME_ACTIVITY_OTHERS	= "";
	
	//	Handler messages
	public final static int MESSAGE_FIN_TIEMPO 		= -1;
	public final static int MESSAGE_CHANGE_SECONDS 	= 1;
	
}
