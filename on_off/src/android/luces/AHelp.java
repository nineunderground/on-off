/**
On-Off Android app. Basic game where you must switch off all lights
    Copyright (C) 2010  nineunderground@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package android.luces;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.graphics.drawable.AnimationDrawable;
import android.luces.utils.Navigation;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class AHelp extends Activity {

	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        
        // Quitamos la barra de titulo
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        // Sin barra de notificaciones
        Navigation.setFullScreenMode(this);
        
        //setContentView(R.layout.help);
        setContentView(R.layout.help_pruebas);
        
        cargarEstilos();
        
        cargarEventos();
        
        setContentLanguage();
        
    }

    /**
     * Se cargan los eventos de la pantalla
     */
    private void cargarEventos() {
		
    	Button	btnVolver	=	(Button) findViewById(R.id.boton_volver_de_ayuda);
    	
    	btnVolver.setOnTouchListener(
    			
    			new View.OnTouchListener() {
					
					public boolean onTouch(View arg0, MotionEvent event) {
						
						if ( event.getAction() == MotionEvent.ACTION_UP ) {
							
							// Se para la animacion
							//ImageView	marcoAnimacion	=	(ImageView) findViewById(R.id.animacion_lampara);
							StopAnimation	finAnim	=	new StopAnimation();
							
							Timer task	=	new Timer(false);
							task.schedule(finAnim, 1);
							
							// Se vuelve al menu principal
							
							setResult(AHelp.RESULT_OK);
							finish();
						
							return true;
							
						}
						
						return false;
					}
				}
    			
    	);
    	
	}

	/** 
     * Se cargan los estilos pertinentes para la pantalla
     */
	private void cargarEstilos() {
		
		// Se pone la animacion del muñeco que enciende la lampara
		
		ImageView	marcoAnimacion	=	(ImageView) findViewById(R.id.animacion_lampara);
		
		marcoAnimacion.setBackgroundResource(R.anim.animation);
		
		marcoAnimacion.setLayoutParams(marcoAnimacion.getLayoutParams());
		
		Log.d(this.getClass().getName(), "ANIMACION CARGADA!!! \n");
		
		StartAnimation	inicioAnim	=	new StartAnimation();
		Timer task	=	new Timer(false);
		
		Log.d(this.getClass().getName(), "ANIMACION INICIADA!!! \n");
		task.schedule(inicioAnim, 1000);
		
		//Log.d(this.getClass().getName(), "ESTA CORRIENDO LA ANIMACION? " + animacion.isRunning()  + " \n");
		
		//Log.d(this.getClass().getName(), "UNA SOLA VEZ? " + animacion.isOneShot()  + " \n");
		
	}
	
	//**************************************************************************************************************************************
    //
    //									T R A D U C T I O N
    //
    //**************************************************************************************************************************************

	/**
     * Sets strings according language supplied to this function
     * 
     */
    private void setContentLanguage() {
		
    	Button		botonVolver		=	(Button)findViewById(R.id.boton_volver_de_ayuda);
    	TextView	tituloObjetivo	=	(TextView)findViewById(R.id.tituloObjetivo);
    	TextView	comentobjetivo	=	(TextView)findViewById(R.id.objetivo);
    	TextView	tituloIntruc	=	(TextView)findViewById(R.id.tituloInstrucciones);
    	TextView	comentIntruc	=	(TextView)findViewById(R.id.instrucciones);
    	TextView	tituloPuntuac	=	(TextView)findViewById(R.id.tituloPuntuacion);
    	TextView	comentPuntuac	=	(TextView)findViewById(R.id.puntuacion);
    	
    	botonVolver.setText(Navigation.traducir(ALuces.getDiccionario(ALuces.language), "BOTON_VOLVER"));
    	tituloObjetivo.setText(Navigation.traducir(ALuces.getDiccionario(ALuces.language), "TITULO_OBJETIVO"));
    	comentobjetivo.setText(Navigation.traducir(ALuces.getDiccionario(ALuces.language), "COMENTARIO_OBJETIVO"));
    	tituloIntruc.setText(Navigation.traducir(ALuces.getDiccionario(ALuces.language), "TITULO_INSTRUCCIONES"));
    	comentIntruc.setText(Navigation.traducir(ALuces.getDiccionario(ALuces.language), "COMENTARIO_INSTRUCCIONES"));
    	tituloPuntuac.setText(Navigation.traducir(ALuces.getDiccionario(ALuces.language), "TITULO_PUNTUACION"));
    	comentPuntuac.setText(Navigation.traducir(ALuces.getDiccionario(ALuces.language), "COMENTARIO_PUNTUACION"));
    	
	}	
	
	//**************************************************************************************************************************************
    //
    //									V O L V E R    S I N    B O T O N    B A C K
    //
    //**************************************************************************************************************************************

	/**
	 * Si se pulsa el boton back no se hace nada.
	 */
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			Log.d(this.getClass().getName(), "back button pressed");
		}
		
		return false;
	}	
	
	/**
	 * Se inicia la animacion desde fuera del Activity. Es obligatorio hacerlo asi
	 * @author nakki
	 *
	 */
	class StartAnimation extends TimerTask{
		
		public void run(){
		
			ImageView	marcoAnimacion	=	(ImageView) findViewById(R.id.animacion_lampara);
			AnimationDrawable animacion	=	(AnimationDrawable)marcoAnimacion.getBackground();
			
			animacion.start();
			
		}
		
	}
	
	/**
	 * Se para la animacion desde fuera del Activity. Es obligatorio hacerlo asi
	 * @author nakki
	 *
	 */
	class StopAnimation extends TimerTask{
		
		public void run(){
		
			ImageView	marcoAnimacion	=	(ImageView) findViewById(R.id.animacion_lampara);
			AnimationDrawable animacion	=	(AnimationDrawable)marcoAnimacion.getBackground();
			
			animacion.stop();
			
		}
		
	}
	
}
